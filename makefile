CC=gcc
CFLAGS=-I. -std=c99
SERIAL_DEP = src/serial/serial.h
DH_DEP = src/dh/dh.h
AES_DEP = src/aes/aes.h src/aes/s_box.h src/aes/sha256.h
AVALON_DEP = src/avalon/avalon-interface/avalon-interface.h src/avalon/lookup-table/lookup-table.h
DEPS = dep/argtable/argtable3.h src/secure-chat/secure-chat.h

SERIAL_OBJ = src/serial/serial.o
DH_OBJ = src/dh/hostA.o src/dh/hostB.o src/dh/dh.o
AES_OBJ = src/aes/aes.o src/aes/s_box.o src/aes/sha256.o
AVALON_OBJ = src/avalon/avalon-interface/avalon-interface.o src/avalon/lookup-table/lookup-table.o
OBJ = src/main.o dep/argtable/argtable3.o src/secure-chat/secure-chat.o

%.o: %.c $(DEPS) $(AVALON_DEP) $(AES_DEP) $(DH_DEP) $(SERIAL_DEP)
	$(CC) -c -o $@ $< $(CFLAGS)

secure-chat-app: $(OBJ) $(AVALON_OBJ) $(AES_OBJ) $(DH_OBJ) $(SERIAL_OBJ)
	$(CC) -o $@ $^ $(CFLAGS) -lm