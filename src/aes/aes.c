/*
 * AES is a 128 bit block cipher:
 *      - Takes a 128-bit (16 byte) long message
 *      - Encrypts it into a 128-bit (16 byte) long ciphertext w/ a key
 *      - This key can be 128-bit (16 byte), 192-bit (24 byte) or 256-bit (32 byte)
 */
#include "aes.h"
#include "s_box.h"
#include <string.h>

uint8_t rcon[10] = {
    0x01, 0x02, 0x04, 0x08, 0x10,
    0x20, 0x40, 0x80, 0x1b, 0x36};

uint8_t colMixer[16] = {
    0x02, 0x03, 0x01, 0x01,
    0x01, 0x02, 0x03, 0x01,
    0x01, 0x01, 0x02, 0x03,
    0x03, 0x01, 0x01, 0x02};

uint8_t invColMixer[16] = {
    0x0E, 0x0B, 0x0D, 0x09,
    0x09, 0x0E, 0x0B, 0x0D,
    0x0D, 0x09, 0x0E, 0x0B,
    0x0B, 0x0D, 0x09, 0x0E};
/*
 * Multiplication in GF(2^8)
 * http://en.wikipedia.org/wiki/Finite_field_arithmetic
 */
// Could be replaced with a lookup table
uint8_t gmult(uint8_t a, uint8_t b) {
	uint8_t p = 0, i = 0, hbs = 0;
	for (i = 0; i < 8; i++) {
		if (b & 1) {
			p ^= a;
		}
		hbs = a & 0x80;
		a <<= 1;
		if (hbs) a ^= 0x1b;
		b >>= 1;
	}
	return (uint8_t)p;
}

/*
* The purpose of the extended key is to change the encryption byte mapping from 
* 1 to 1 to one to many (in this case 1 to 16 for each block of message)
* Otherwise, the middle mallacious listener knows the input and outputs of encryption which is easier to analyze
*/

// Initializes an expanded key by allocating memory to the key to be generated
uint8_t **init_key_set(uint8_t *originalKey) {
    uint8_t **copyKeySet = malloc(sizeof(uint8_t *) * MIX_ROUND + 1);
    for(int i = 0; i <= MIX_ROUND; i++) {
        copyKeySet[i] = malloc(sizeof(originalKey));
    }
    for(int i = 0; i < ROW * COLUMN; i++) {
        copyKeySet[0][i] = originalKey[i];
    }
    return copyKeySet;
}

// Encrypt a clear text of 16 characters
void aes_encrypt(uint8_t *clearText, uint8_t **keySet) {
    printf("This is the input clear text:\n");
    print_key_c((char*) clearText);
    printf("\n");
    add_round_key(clearText, keySet[0]);
    for(int i = 1; i < MIX_ROUND; i++) {
        substitute_bytes(clearText);
        shift_rows(clearText);
        mix_cols(clearText);
        add_round_key(clearText, keySet[i]);
    }
    substitute_bytes(clearText);
    shift_rows(clearText);
    add_round_key(clearText, keySet[10]);
    printf("This is the encrypted text:\n");
    print_key_c((char*) clearText);
    printf("\n");
    printf("This is the encrypted text in hex:\n");
    print_uint8_mtrx(clearText);
    printf("\n");
}

void aes_encrypt_full_message(int messageSize, uint8_t *clearText, uint8_t **keySet){
    int padBytes = (messageSize%16)==0 ? 0: 16-(messageSize%16);
	printf("\n----------------------FULL ENCRYPT FUNCTION START ---------------------\n");
	/* Size of padded message is the size of input + enough padding to make it 
	   divisible by 16*/
	char paddedMessage[messageSize + padBytes];
	memcpy(paddedMessage, clearText, messageSize);
	
    /* Adds padding to right-side of array if needed */
    for(int i = sizeof paddedMessage-1; i >= (sizeof paddedMessage)-padBytes; i--){
        paddedMessage[i] = ' ';
    }
    
    printf("This is the padded message with char *: \n");
    print_key_c((char*) paddedMessage);

    /* Encrypt message in chunks */
    for(int j = 0; j<messageSize; j=j+16){
        aes_encrypt((uint8_t *)paddedMessage+j, keySet);
    }
    memcpy(clearText, paddedMessage, messageSize + padBytes);

    printf("\nsize of padded message is %d\n", sizeof paddedMessage);
    printf("padded bytes is %d\n", padBytes);
    printf("%s\n", paddedMessage);
}

// Decrypts a cipher text of 16 characters
void aes_decrypt(uint8_t *cipherText, uint8_t **keySet) {
    // print_uint8_mtrx(cipherText);
    // printf("KEYSET [10]:\n");
    // print_uint8_mtrx(keySet[10]);
    // print_uint8_mtrx(cipherText);
    add_round_key(cipherText, keySet[MIX_ROUND]);
    //printf("Added the first round key\n");
    //print_uint8_mtrx(cipherText);
    for(int i = MIX_ROUND - 1; i > 0; i--) {
        //printf("INV Shift Rows: %d\n", i);
        inv_shift_rows(cipherText);
        //print_uint8_mtrx(cipherText);
        //printf("INV Sub Bytess: %d\n", i);
        inv_substitute_bytes(cipherText);
       // print_uint8_mtrx(cipherText);
        //printf("Add Round Key: %d\n", i);
        add_round_key(cipherText, keySet[i]);
        //print_uint8_mtrx(cipherText);
       // printf("INV Mix Cols: %d\n", i);
        inv_mix_cols(cipherText);
        //print_uint8_mtrx(cipherText);
    }
    inv_shift_rows(cipherText);
    inv_substitute_bytes(cipherText);
    add_round_key(cipherText, keySet[0]);
    printf("This is the decrypted text:\n");
    print_key_c((char*)cipherText);
}

void aes_decrypt_full_message(int messageSize, uint8_t *clearText, uint8_t **keySet){
	printf("\n----------------------FULL DECRYPT FUNCTION START ---------------------\n");

    /* Decrypt message in chunks */
    for(int j = 0; j<messageSize; j=j+16){
        aes_decrypt((uint8_t *)clearText+j, keySet);
    }

}

void expand_key(uint8_t **keySet) {
    for(int round = 0; round < MIX_ROUND; round++) {
        rot_word(keySet[round], keySet[round+1]);
        substitute_col(keySet[round+1]);
        xor_key_rcon(keySet[round], keySet[round+1], round);
        xor_key_cols(keySet[round], keySet[round+1]);
    }
}

// Rolls the final column by 1
void rot_word(uint8_t *keyZero, uint8_t *keyOne) {
    for(int i = 0; i < COLUMN; i++) {
        keyOne[(ROW * (i + 3)) % (ROW * COLUMN)] = keyZero[ROW * (i + 1) - 1];
    }
}

// XOR's two columns with RCON and returns the result array
void xor_key_rcon(uint8_t *keyZero, uint8_t *keyOne, int round) {
    for(int i = 0; i < ROW; i++) {
        keyOne[i * COLUMN] = (keyZero[i * COLUMN] ^ keyOne[i * COLUMN]) ^ (i == 0 ? rcon[round] : 0x00);
    }
}

// XOR's the remaining columns during key expansion
void xor_key_cols(uint8_t *keyZero, uint8_t *keyOne) {
    for(int i = 1; i < COLUMN; i++) {
        for(int j = 0; j < ROW; j++){
            keyOne[j*ROW + i] = (keyOne[j*ROW + i - 1] ^ keyZero[j*ROW + i]);
        }
    }
}

void shift_left(uint8_t *input, int row) {
    uint8_t tmp;
    for(int i = 0; i < row; i++) {
        tmp = input[row*COLUMN];
        for(int j = 0; j < COLUMN; j++) {
            input[row*COLUMN + j] = input[row*COLUMN + j + 1];
        }
        input[row*COLUMN + COLUMN - 1 ] = tmp;
    }
}

void shift_right(uint8_t *input, int row) {
    uint8_t tmp;
    for(int i = 0; i < row; i++) {
        tmp = input[row*COLUMN + COLUMN - 1];
        for(int j = COLUMN - 1; j >= 0; j--) {
            input[row*COLUMN + j] = input[row*COLUMN + j - 1];
        }
        input[row*COLUMN] = tmp;
    }
}

// Shifts rows
void shift_rows(uint8_t *input) {
    for(int i = 0; i < ROW; i++) {
        shift_left(input, i);
    }
}

// Inverses shifted rows
void inv_shift_rows(uint8_t *input) {
    for(int i = 0; i < ROW; i++) {
        shift_right(input, i);
    }
}

// Mix Columns
void mix_cols(uint8_t *input) {
    uint8_t tmp[16];
    for(int i = 0; i < ROW; i++) {    
        for(int j = 0; j < COLUMN; j++) {   
            tmp[i*ROW + j]=0;
            for(int k = 0; k < COLUMN; k++) {
                tmp[i*ROW + j] ^= gmult(colMixer[i*ROW + k], input[k*ROW + j]);    
            }    
        }
    }
    for(int i = 0; i < ROW * COLUMN; i++) {
        input[i] = tmp[i];
    }
}

void inv_mix_cols(uint8_t *input) {
    uint8_t tmp[16];
    for(int i = 0; i < ROW; i++) {    
        for(int j = 0; j < COLUMN; j++) {   
            tmp[i*ROW + j]=0;
            for(int k = 0; k < COLUMN; k++) {
                tmp[i*ROW + j] ^= gmult(invColMixer[i*ROW + k], input[k*ROW + j]);    
            }    
        }
    }
    for(int i = 0; i < ROW * COLUMN; i++) {
        input[i] = tmp[i];
    }
}

// XOR's the input text with round key
void add_round_key(uint8_t *input, uint8_t *roundKey) {
    for(int i = 0; i < COLUMN * ROW; i++) {
            //printf("i: %d\n", i);
            //printf("Before: %x\n",input[i]);
            //printf("Xord with: %x\n", roundKey[i]);
            input[i] = input[i] ^ roundKey[i];
           // printf("After: %x\n",input[i]);
    }
}

// For dev purposes only, prints the given hex-based key on terminal
void print_tmpCol(uint8_t *tmpCol) {
    for(int i = 0; i < COLUMN; i++) {
        printf("%x\n", tmpCol[i]);
    }
}
// For dev purposes only, prints the given hex-based key on terminal
void print_uint8_mtrx(uint8_t *key) {
    for(int i = 0; i < ROW; i++) {
        for(int j = 0; j < COLUMN; j++) {
            printf("%x\t", key[ROW*i + j]);
        }
        printf("\n");
    }
}

// For dev purposes only, prints the given hex-based key on terminal
void print_key_c(char *key) {
    for(int i = 0; i < ROW; i++) {
        for(int j = 0; j < COLUMN; j++) {
            printf("%c", key[ROW*i + j]);
        }
    }
    printf("\n");
}

void print_uint8_keyset(uint8_t **keySet, int keySetSize) {
    for(int i = 0; i < ROW; i++) {
        for(int x = 0; x < keySetSize; x++) {
            for(int j = 0; j < COLUMN; j++) {
                printf("%x ", keySet[x][ROW*i + j]);
                if(j == COLUMN - 1) {
                    printf("\t");
                }
            }
        }
        printf("\n");
    }
}

int itoa(int value,char *ptr)
     {
        int count=0,temp;
        if(ptr==NULL)
            return 0;   
        if(value==0)
        {   
            *ptr='0';
            return 1;
        }

        if(value<0)
        {
            value*=(-1);    
            *ptr++='-';
            count++;
        }
        for(temp=value;temp>0;temp/=10,ptr++);
        *ptr='\0';
        for(temp=value;temp>0;temp/=10)
        {
            *--ptr=temp%10+'0';
            count++;
        }
        return count;
}

const uint64_t nofdigits(uint64_t value) {
	// give room for null char at end
	int digits = 1;
	while(value != 0) {
		value = value / 10;
		digits++;
	}
	return digits;
}
