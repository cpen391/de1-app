#include "s_box.h"
#define S_COLUMNS 16
#define S_ROWS 16
#define COLUMNS 4
#define ROWS 4


// Substitues the values of the column based on the provided s-box
void substitute_col(uint8_t *keyOne) {
	int leftNibble, rightNibble;
	for(int i = 0; i < COLUMNS; i++) {
		leftNibble = (keyOne[COLUMNS * i] >> 4) & 0x0F;
		rightNibble = (keyOne[COLUMNS * i] & 0x0F);
		keyOne[COLUMNS * i] = s_box[leftNibble * S_COLUMNS + rightNibble];
	}
}

void substitute_bytes(uint8_t *clearText) {
	int leftNibble, rightNibble;
	for(int i = 0; i < COLUMNS * ROWS; i++) {
		leftNibble = (clearText[i] >> 4) & 0x0F;
		rightNibble = (clearText[i] & 0x0F);
		clearText[i] = s_box[leftNibble * S_COLUMNS + rightNibble];
	}
}

void inv_substitute_bytes(uint8_t *clearText) {
	int leftNibble, rightNibble;
	for(int i = 0; i < COLUMNS * ROWS; i++) {
		leftNibble = (clearText[i] >> 4) & 0x0F;
		rightNibble = (clearText[i] & 0x0F);
		clearText[i] = inv_s_box[leftNibble * S_COLUMNS + rightNibble];
	}
}


// Dev purposes only; prints the s-box
void print_s_box() {
    for(int i = 1; i <= 256; i++){
        printf("%x\t", s_box[i-1]);
        if(i % 16 == 0) {
            printf("\n");
        }
    }
}