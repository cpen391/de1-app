#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#define ROW 4
#define COLUMN 4
#define MIX_ROUND 10

uint8_t **init_key_set(uint8_t *original_key);
void aes_encrypt(uint8_t *clearText, uint8_t **keySet);
void aes_decrypt(uint8_t *cypherText, uint8_t **keySet);
void shift_rows(uint8_t *input);
void inv_shift_rows(uint8_t *input);
void aes_encrypt_full_message(int messageSize, uint8_t *clearText, uint8_t **keySet);
void aes_decrypt_full_message(int messageSize, uint8_t *clearText, uint8_t **keySet);

void mix_cols(uint8_t *input);
void inv_mix_cols(uint8_t *input);
void add_round_key(uint8_t *input, uint8_t *roundKey);
void expand_key(uint8_t **key_set);
void rot_word(uint8_t *key_zero, uint8_t *key_one);
void xor_key_rcon(uint8_t *key_zero, uint8_t *key_one, int round);
void xor_key_cols(uint8_t *key_zero, uint8_t *key_one);
void print_tmpCol(uint8_t *tmpCol);
void print_key_c(char *key);
void print_uint8_keyset(uint8_t **expandedKeySet, int size);
void print_uint8_mtrx(uint8_t *key);
int itoa(int value,char *ptr);
const uint64_t nofdigits(uint64_t value);