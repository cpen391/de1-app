#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <limits.h>
#include <termios.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h> 
#include <ctype.h>
#include "../aes/aes.h"
#include "../aes/sha256.h"

#define MAX_LEN 1024
#define MAXSIZE 1000000
#define CAESAR_MOD 41
#define M_ITERATION 15

typedef struct GlobalInfo {
	int prime;
	int generator;
} GlobalInfo;

int compute_exp_modulo(int a, int b, int p);
int GeneratePrime();
int GeneratePrimitiveRoot(int p);
int MillerRabinTest(int value, int iteration);
int runHostA();
int runHostB();