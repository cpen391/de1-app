#include "dh.h"
#include "../serial/serial.h"

/* HostB main program */
int runHostB() {
	const char *portname = "/dev/cu.usbserial-1440";
	srand(time(NULL));
	// Choose the baud rate (bits per second).  This does not matter if you are
	uint32_t baud_rate = 38400;
	printf("Opening portB\n");
	int fd = open_serial_port(portname, baud_rate);
	if (fd < 0) { return 1; }
	printf("Opened serial port, starting DH-Key XChange\n");
	char message_size[3];
	uint64_t fileIndex = 0;
	int recv_size;
	/* Generate a prime number and its primitive root (publicly known) */
	printf("B: Generated number of keys:\n");
	for(uint64_t i = 0; i < 100000000; i++) {
		// printf("YOLO\n");
		printf("\r%llu", i);
		GlobalInfo g;	
		// printf("YOLO1\n");
		g.prime = GeneratePrime();
		g.generator = GeneratePrimitiveRoot(g.prime);
		// printf("YOLO4\n");
		/* Choose a private key for the HostB */
		int private_key = rand() % (g.prime - 1) + 1;
		int public_key = compute_exp_modulo(g.generator, private_key, g.prime);

		char message[MAX_LEN];
		memset(message, 0, sizeof(message));	 
		int n = sprintf(message, "%d\n%d\n%d\n", public_key, g.prime, g.generator);
		sprintf(message_size, "%d", n);
		for(int x = n; x < 3; x++) {
			message_size[x] =  ' ';
		}
		// printf("YOLO2\n");
		send_message(fd, (uint8_t *)message_size, 3);
		send_message(fd, (uint8_t *)message, n);
		/* Receive HostA public key */ 
		n = recv_message(fd, (uint8_t *)message_size, 3);
		recv_size = atoi(message_size);
		n = recv_message(fd, (uint8_t *)message, recv_size);
		int public_key_server = atoi(message);
		
		// printf("YOL3\n");
		/* Compute shared key and AES key */
		int shared_key = compute_exp_modulo(public_key_server, private_key, g.prime);
		sha256_context ctx;
		uint8_t hv[32];
        sha256_init(&ctx);
		const uint64_t arr_size = nofdigits(shared_key);
		char sharedKeyArray[arr_size];
		int size = itoa(shared_key, sharedKeyArray);
        sha256_hash(&ctx, (uint8_t *)sharedKeyArray, (uint32_t)size);
        sha256_done(&ctx, hv);
		uint8_t AESkey[16];

		// XOR'ing two Halves of the SHA256 hash value
		for(int x = 0; x < 16; x++) {
			AESkey[x] = hv[x] ^ hv[x + 16];
		}
		char fileName[64];
		sprintf(fileName, "%llu", fileIndex);
		char fullName[128]  = "aeskeys1/";
		strcat(fullName,fileName);

		FILE *outFile;
		outFile = fopen(fullName, "w");
		if (outFile == NULL) {
			perror("Failed: ");
			return 1;
		}

		for(int x = 0; x < 16; x++) {
			fprintf(outFile, "%02x", AESkey[x]);
		}
		fclose(outFile);
		fileIndex++;
	}
	return 0;
}
