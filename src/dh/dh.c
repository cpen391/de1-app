#include "dh.h"

/* Generate the primitive root by checking for random numbers */
int GeneratePrimitiveRoot(int p) {
	// printf("GeneratePrimitiveRoot\n");
	/* Construct sieve of primes */
	int sieve[MAXSIZE];
	memset(sieve, 0, sizeof(sieve));
	sieve[0] = sieve[1] = 1;
	for (int i = 4; i < MAXSIZE; i += 2)
		sieve[i] = 1;
	for (int i = 3; i < MAXSIZE; i += 2) {
		if (!sieve[i]) {
			for (int j = 2 * i; j < MAXSIZE; j += i)
				sieve[j] = 1;
		}
	}
	while (1) {
		int a = rand() % (p - 2) + 2;
		int phi = p - 1, flag = 1, root = sqrt(phi);
		for (int i = 2; i <= root; i++) {
			if (!sieve[i] && !(phi % i)) {
				int mod_result = compute_exp_modulo(a, phi / i, p);
				if (mod_result == 1) {
					flag = 0;
					break;
				}
				if (MillerRabinTest(phi / i, M_ITERATION) && !(phi % (phi / i))) {
					int mod_result = compute_exp_modulo(a, phi / (phi / i), p);
					if (mod_result == 1) {
						flag = 0;
						break;
					}
				}
			}
		}
		if (flag) 
			return a;
	}
}

/* Generate a prime number that is going to be shared 
 * globally between HostB and HostA
 */
int GeneratePrime() {
	// printf("GeneratePrime\n");
	while(1) {
		// printf("Miller arabimm\n");
		int current_value = rand() % INT_MAX;
		if (!(current_value % 2)) {
			// printf("Current val++ %d\n", current_value);
			current_value++;
		}
		if (MillerRabinTest(current_value, M_ITERATION) == 1) {
			// printf("Miller arabimm\n");
			// printf("Returning Current Val %d\n", current_value);
			return current_value;
		}
	}
}

/* Function to check primality of random generated numbers using Miller-Rabin Test */
int MillerRabinTest(int value, int iteration) {
	if (value < 2)
		return 0;
	int q = value - 1, k = 0;
	while (!(q % 2)) {
		q /= 2;
		k++;
	}
	for (int i = 0; i < iteration; i++) {
		int a = rand() % (value - 1) + 1;
		int current = q;
		int flag = 1;
		int mod_result = compute_exp_modulo(a, current, value);
		for (int i = 1; i <= k; i++) {
			if (mod_result == 1 || mod_result == value - 1) {
				flag = 0;
				break;
			}
			mod_result = (int)((long long)mod_result * mod_result % value);
		}
		if (flag)
			return 0;
	}
	return 1;
}

int compute_exp_modulo(int a, int b, int p) {
	long long x = 1, y = a;
	while (b > 0) {
		if (b % 2 == 1)
			x = (x * y) % p;
		y = (y * y) % p;
		b /= 2;
	}
	return (int)(x % p);
}