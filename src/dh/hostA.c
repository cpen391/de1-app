#include "dh.h"
#include "../serial/serial.h"

/* HostA main program */
int runHostA() {
	GlobalInfo g;
	// Establish the serial connection with the HC-05 module
	const char *portname = "/dev/cu.usbserial-1420";
	
	// Choose the baud rate (bits per second).
	uint32_t baud_rate = 38400;
	printf("Opening portA\n");
	int fd = open_serial_port(portname, baud_rate);
	if (fd < 0) { return 1; }
	printf("opened serial \n");
	uint64_t fileIndex = 0;
	char buffer[MAX_LEN];
	char message_size[3];
	int private_key, public_key, shared_key, public_key_client, n, i;
	int recv_size;

	printf("A: Generated number of keys:\n");
	while (fileIndex < 100000000) {
		// printf("XD\n");
		printf("\r%llu", fileIndex);
		memset(buffer, 0, sizeof(buffer));
		/* Receive the message size that will come after*/
		n = recv_message(fd, (uint8_t *)message_size, 3);
		recv_size = atoi(message_size);
		/* Receive key, generator and prime from client */
		n = recv_message(fd, (uint8_t *)buffer, recv_size);
		public_key_client = atoi(buffer);
		i = 0;
		while (buffer[i] != '\n')
			i++;
		g.prime = atoi(buffer + ++i);
		while (buffer[i] != '\n')
			i++;
		g.generator = atoi(buffer + ++i);
		private_key = rand() % (g.prime - 1) + 1;
		public_key = compute_exp_modulo(g.generator, private_key, g.prime);

		/* Send public_key to the client */
		memset(buffer, 0, sizeof(buffer));
		n = sprintf(buffer, "%d\n", public_key);
		sprintf(message_size, "%d", n);
		for(int x = n; x < 3; x++) {
			message_size[x] =  ' ';
		}
		send_message(fd, (uint8_t *)message_size, 3);
		send_message(fd, (uint8_t *)buffer, n); 

		/* Compute shared value and AES key */
		shared_key = compute_exp_modulo(public_key_client, private_key, g.prime);
		sha256_context ctx;
		uint8_t hv[32];
        sha256_init(&ctx);
		const uint64_t arr_size = nofdigits(shared_key);
		char sharedKeyArray[arr_size];
		int size = itoa(shared_key, sharedKeyArray);
        sha256_hash(&ctx, (uint8_t *)sharedKeyArray, (uint32_t)size);
        sha256_done(&ctx, hv);
		uint8_t AESkey[16];
		// XOR'ing two Halves of the SHA256 hash value
		for(int x = 0; x < 16; x++) {
			AESkey[x] = hv[x] ^ hv[x + 16];
		}

		/*Create a file with the index and write the AES key inside it*/
		char fileName[64];
		sprintf(fileName, "%llu", fileIndex);
		char fullName[128]  = "aeskeys/";
		strcat(fullName,fileName);
		FILE *outFile;
		outFile = fopen(fullName, "w");
		if (outFile == NULL) {
			perror("Failed: ");
			return 1;
		}
		for(int x = 0; x < 16; x++) {
			fprintf(outFile, "%02x", AESkey[x]);
		}
		fclose(outFile);
		fileIndex++;
	}
	return 0;
}
