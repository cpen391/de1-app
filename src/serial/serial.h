#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <limits.h>
#include <termios.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h> 
#include <ctype.h>

int open_serial_port(const char * device, uint32_t baud_rate);
int send_message(int fd, uint8_t * buffer, size_t size);
ssize_t recv_message(int fd, uint8_t * buffer, size_t size);