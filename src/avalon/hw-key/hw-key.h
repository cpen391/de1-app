#ifndef HW_KEY_H
#define HW_KEY_H

#include <stddef.h>

#define HW_KEY_BASE 0x00000060

int init_hw_key(void);
int get_hw_key(uint32_t *key);

#endif