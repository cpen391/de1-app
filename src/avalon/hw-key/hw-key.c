#include "hw-key.h"
#include "../avalon-interface/avalon-interface.h"
#include <stddef.h>

static volatile uint32_t *h2p_lw_hw_key_addr=NULL;

int init_hw_key(void)
{

    void *virtual_base = get_avalon_virtual_base();
    if (virtual_base == NULL) {
        return 1;
    }

    h2p_lw_hw_key_addr=(uint32_t*)(virtual_base + 
        ((HW_KEY_BASE) & (HW_REGS_MASK)));
    return 0;
}

int get_hw_key(uint32_t *key)
{
    return 0;
}