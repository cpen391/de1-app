#include <src/avalon/lookup-table/lookup-table.h>
#include <src/avalon/avalon-interface/avalon-interface.h>
#include <stddef.h>
#include <stdint.h>

static volatile uint32_t *h2p_lw_lookup_addr=NULL;

int init_lookup_table(void)
{
    void *virtual_base = get_avalon_virtual_base();
    if (virtual_base == NULL) {
        return 1;
    }

    h2p_lw_lookup_addr = (uint32_t*)(virtual_base + 
        ((LOOKUP_TABLE_BASE) & (HW_REGS_MASK)));
    
    return 0;
}

int table_reset(void)
{
    uint32_t res = *(h2p_lw_lookup_addr + RESET_OFFSET);
    if (res != REQUEST_RESET_MAGIC) {
        return 1;
    }

    return 0;
}

int table_regen(void)
{
    uint32_t res = *(h2p_lw_lookup_addr + REGEN_OFFSET);
    if (res != REQUEST_REGEN_MAGIC) {
        return 1;
    }

    return 0;
}

int table_lookup(uint32_t *key)
{
    *key = *(h2p_lw_lookup_addr + LOOKUP_OFFSET);
    return 0;
}

int write_seed(int64_t s0, int64_t s1)
{
    uint32_t s0_high = (uint32_t)(s0 >> 32);
    uint32_t s0_low = (uint32_t)s0;
    uint32_t s1_high = (uint32_t)(s1 >> 32);
    uint32_t s1_low = (uint32_t)s1;

    *(h2p_lw_lookup_addr + S0_HIGH_OFFSET) = s0_high;
    if (*(h2p_lw_lookup_addr + S0_HIGH_OFFSET) != s0_high) {
        return 1;
    }
    *(h2p_lw_lookup_addr + S0_LOW_OFFSET) = s0_low;
    if (*(h2p_lw_lookup_addr + S0_LOW_OFFSET) != s0_low) {
        return 1;
    }
    *(h2p_lw_lookup_addr + S1_HIGH_OFFSET) = s1_high;
    if (*(h2p_lw_lookup_addr + S1_HIGH_OFFSET) != s1_high) {
        return 1;
    }
    *(h2p_lw_lookup_addr + S1_LOW_OFFSET) = s1_low;
    if (*(h2p_lw_lookup_addr + S1_LOW_OFFSET) != s1_low) {
        return 1;
    }

    return 0;
}