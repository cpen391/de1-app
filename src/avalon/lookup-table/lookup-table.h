#ifndef LOOKUP_TABLE_H
#define LOOKUP_TABLE_H

#include <stdint.h>

#define LOOKUP_TABLE_BASE   0x00000060
#define REQUEST_RESET_MAGIC 0xDEDEDEDE
#define REQUEST_REGEN_MAGIC 0xABABABAB

#define S0_HIGH_OFFSET      0x0
#define S0_LOW_OFFSET       0x1
#define S1_HIGH_OFFSET      0x2
#define S1_LOW_OFFSET       0x3
#define REGEN_OFFSET        0x4
#define LOOKUP_OFFSET       0x5
#define RESET_OFFSET        0x6

int init_lookup_table(void);
int table_regen(void);
int table_reset(void);
int table_lookup(uint32_t *key);
int write_seed(int64_t s0, int64_t s1);

#endif

