#include <fcntl.h>
#include <sys/mman.h>
#include <src/avalon/avalon-interface/avalon-interface.h>
#include <stdio.h>
#include <unistd.h>

static void *virtual_base = NULL;
static int fd;

int init_avalon(void)
{
    // Open /dev/mem
	if ((fd = open( "/dev/mem", (O_RDWR | O_SYNC))) == -1) {
		printf("ERROR: could not open \"/dev/mem\"...\n");
		return 1;
	}

    // get virtual addr that maps to physical
	virtual_base = mmap(NULL, HW_REGS_SPAN, (PROT_READ | PROT_WRITE),
        MAP_SHARED, fd, HW_REGS_BASE);
	if( virtual_base == MAP_FAILED ) {
		printf("ERROR: mmap() failed...\n");
		close(fd);
		return 1;
	}

    return 0;
}

int teardown_avalon(void)
{
    if(munmap(virtual_base, HW_REGS_SPAN) != 0) {
		printf("ERROR: munmap() failed...\n");
		close(fd);
		return 1;
	}

	close(fd);
    return 0;
}

void* get_avalon_virtual_base(void)
{
    return virtual_base;
}