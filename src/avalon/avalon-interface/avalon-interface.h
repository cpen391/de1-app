#ifndef AVALON_INTERFACE_H
#define AVALON_INTERFACE_H

#define HW_REGS_SPAN ( 0x00200000 )
#define HW_REGS_BASE ( 0xff200000 )
#define HW_REGS_MASK ( HW_REGS_SPAN - 1 )

int init_avalon(void);
int teardown_avalon(void);
void* get_avalon_virtual_base(void);

#endif