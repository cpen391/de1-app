
#include "AT.h"

int
set_interface_attribs (int fd, int speed, int parity)
{
        struct termios tty;
        if (tcgetattr (fd, &tty) != 0)
        {
                printf("error %d from tcgetattr", errno);
                return -1;
        }

        cfsetospeed (&tty, speed);
        cfsetispeed (&tty, speed);

        tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;     // 8-bit chars
        // disable IGNBRK for mismatched speed tests; otherwise receive break
        // as \000 chars
        tty.c_iflag &= ~IGNBRK;         // disable break processing
        tty.c_lflag = 0;                // no signaling chars, no echo,
                                        // no canonical processing
        tty.c_oflag = 0;                // no remapping, no delays
        tty.c_cc[VMIN]  = 0;            // read doesn't block
        tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

        tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

        tty.c_cflag |= (CLOCAL | CREAD);// ignore modem controls,
                                        // enable reading
        tty.c_cflag &= ~(PARENB | PARODD);      // shut off parity
        tty.c_cflag |= parity;
        tty.c_cflag &= ~CSTOPB;
        tty.c_cflag &= ~CRTSCTS;

        if (tcsetattr (fd, TCSANOW, &tty) != 0)
        {
                printf("error %d from tcsetattr", errno);
                return -1;
        }
        return 0;
}

void
set_blocking (int fd, int should_block)
{
        struct termios tty;
        memset (&tty, 0, sizeof tty);
        if (tcgetattr (fd, &tty) != 0)
        {
                printf("error %d from tggetattr", errno);
                return;
        }

        tty.c_cc[VMIN]  = should_block ? 1 : 0;
        tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

        if (tcsetattr (fd, TCSANOW, &tty) != 0)
                printf("error %d setting term attributes", errno);
}

void printATResponse(char *messageBuffer) {
    int i = 0;
    while(messageBuffer[i] != '\r') {
        i++;
    }
    if(messageBuffer[i+1] == '\n') {
        // Received message is valid
        i++;
        messageBuffer[i+1] = '\0';
        printf("%s", messageBuffer);
    } else {
        printf("Received message from the RFS board is corrupt!\n");
    }
}

void runATTerminal(int fd) {
    char ATCommand [100];
    int n = 0;
    while(scanf(" %100[^\n]", ATCommand) != 0) {
        write (fd, ATCommand, strlen(ATCommand));
        usleep ((strlen(ATCommand) + 25) * 100);
        char buf [100];
        n = read (fd, buf, sizeof buf);  // read up to 100 characters if ready to read
        // printATResponse(buf);
        for(int i = 0; i < n; i++) {
            printf("%c", buf[i]);
        }
    }
}

int main(void) {
    char *portnameSlave = "/dev/ttyUSB0";
    int fd_slave = open (portnameSlave, O_RDWR | O_NOCTTY | O_SYNC);
    printf("Opened slave.\n");
    if (fd_slave < 0)
    {
            printf("error %d opening %s: %s", errno, portnameSlave, strerror (errno));
            return -1;
    }
    set_interface_attribs (fd_slave, B38400, 0);  // set speed to 115,200 bps, 8n1 (no parity)
    set_blocking (fd_slave, 0);                // set no blocking
    printf("Slave Resetting bluetooth...\n");
    resetBluetooth(fd_slave);
    printf("Slave State:\n");
    checkState(fd_slave);
    printf("Slave Initializing bluetooth...\n");
    initBluetooth(fd_slave);
    printf("Print version:\n");
    printVersion(fd_slave);
    printf("Slave The bluetooth address:\n");
    getBluetoothAddress(fd_slave);
    printf("Slave Setting role to slave...\n");
    setRole(fd_slave, 0);
    printf("Slave Role:\n");
    checkRole(fd_slave);
}