#include "AT.h"

void checkBluetoothConnection(int fd) {
    write (fd, "AT\r\n", 4);
    //usleep ((4 + 25) * 100);             // sleep enough to transmit the 4 plus
    char buf [100];
    int n = read (fd, buf, sizeof buf);  // read up to 100 characters if ready to read
    for(int i = 0; i < n; i++) {
        printf("%c", buf[i]);
    }
}

void checkState(int fd) {
    write (fd, "AT+STATE?\r\n", 11);
    //usleep ((11 + 25) * 100);
    char buf [100];
    int n = read (fd, buf, sizeof buf);  // read up to 100 characters if ready to read
    for(int i = 0; i < n; i++) {
        printf("%c", buf[i]);
    }
}

void initBluetooth(int fd) {
    write (fd, "AT+INIT\r\n", 9);
    //usleep ((9 + 25) * 100);             // sleep enough to transmit the 4 plus
    char buf [100];
    int n = read (fd, buf, sizeof buf);  // read up to 100 characters if ready to read
    for(int i = 0; i < n; i++) {
        printf("%c", buf[i]);
    }
}

void resetBluetooth(int fd) {
    write (fd, "AT+RESET\r\n", 10);
    //usleep ((10 + 25) * 100);             // sleep enough to transmit the 4 plus
    char buf [100];
    int n = read (fd, buf, sizeof buf);  // read up to 100 characters if ready to read
    for(int i = 0; i < n; i++) {
        printf("%c", buf[i]);
    }
}

void printVersion(int fd) {
    write (fd, "AT+VERSION?\r\n", 13);
    //usleep ((13 + 25) * 100);             // sleep enough to transmit the 4 plus
    char buf [100];
    int n = read (fd, buf, sizeof buf);  // read up to 100 characters if ready to read
    for(int i = 0; i < n; i++) {
        printf("%c", buf[i]);
    }
}

void getBluetoothAddress(int fd) {
    write (fd, "AT+ADDR?\r\n", 10);
    //usleep ((10 + 25) * 100);             // sleep enough to transmit the 4 plus
    char buf [100];
    int n = read (fd, buf, sizeof buf);  // read up to 100 characters if ready to read
    for(int i = 0; i < n; i++) {
        printf("%c", buf[i]);
    }
}

void restoreDefault(int fd) {
    write (fd, "AT+ORGL\r\n", 9);
    //usleep ((9 + 25) * 100);
    char buf [100];
    int n = read (fd, buf, sizeof buf);  // read up to 100 characters if ready to read
    for(int i = 0; i < n; i++) {
        printf("%c", buf[i]);
    }
}

void checkRole(int fd) {
    write (fd, "AT+ROLE?\r\n", 10);
    //usleep ((10 + 25) * 100);
    char buf [100];
    int n = read (fd, buf, sizeof buf);  // read up to 100 characters if ready to read
    for(int i = 0; i < n; i++) {
        printf("%c", buf[i]);
    }
}

// Slave->0 Master->1 Slave-Loop->2
void setRole(int fd, int role) {
    if(role == 0) {
        write (fd, "AT+ROLE=0\r\n", 11);
        //usleep ((11 + 25) * 100);
        char buf [100];
        int n = read (fd, buf, sizeof buf);  // read up to 100 characters if ready to read
        for(int i = 0; i < n; i++) {
            printf("%c", buf[i]);
        }
    } else if(role == 1) {
        write (fd, "AT+ROLE=1\r\n", 11);
        //usleep ((11 + 25) * 100);
        char buf [100];
        int n = read (fd, buf, sizeof buf);  // read up to 100 characters if ready to read
        for(int i = 0; i < n; i++) {
            printf("%c", buf[i]);
        }
    } else {
        write (fd, "AT+ROLE=2\r\n", 11);
        //usleep ((11 + 25) * 100);
        char buf [100];
        int n = read (fd, buf, sizeof buf);  // read up to 100 characters if ready to read
        for(int i = 0; i < n; i++) {
            printf("%c", buf[i]);
        }
    }
}

void setCmode(int fd, int role) {
    if(role == 0) {
        write (fd, "AT+CMODE=0\r\n", 12);
        //usleep ((12 + 25) * 100);
        char buf [100];
        int n = read (fd, buf, sizeof buf);  // read up to 100 characters if ready to read
        for(int i = 0; i < n; i++) {
            printf("%c", buf[i]);
        }
    } else if(role == 1) {
        write (fd, "AT+CMODE=1\r\n", 12);
        //usleep ((12 + 25) * 100);
        char buf [100];
        int n = read (fd, buf, sizeof buf);  // read up to 100 characters if ready to read
        for(int i = 0; i < n; i++) {
            printf("%c", buf[i]);
        }
    }
}

void bind(int fd) {
    write (fd, "AT+BIND=2018,11,202786\r\n", 24);
    //usleep ((24 + 25) * 100);
    char buf [100];
    int n = read (fd, buf, sizeof buf);  // read up to 100 characters if ready to read
    for(int i = 0; i < n; i++) {
        printf("%c", buf[i]);
    }
}

void setUART(int fd, int baud) {
    write (fd, "AT+UART=38400,0,0\r\n", 19);
    //usleep ((19 + 25) * 100);
    char buf [100];
    int n = read (fd, buf, sizeof buf);  // read up to 100 characters if ready to read
    for(int i = 0; i < n; i++) {
        printf("%c", buf[i]);
    }
}