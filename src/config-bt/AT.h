#include <errno.h>
#include <fcntl.h> 
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

void checkBluetoothConnection(int fd);
void initBluetooth(int fd);
void resetBluetooth(int fd);
void printVersion(int fd);
void getBluetoothAddress(int fd);
void restoreDefault(int fd);
void checkRole(int fd);
void setRole(int fd, int role);
void checkState(int fd);
void setCmode(int fd, int role);
void bind(int fd);
void setUART(int fd, int baud);