#include "AT.h"

int
set_interface_attribs (int fd, int speed, int parity)
{
        struct termios tty;
        if (tcgetattr (fd, &tty) != 0)
        {
                printf("error %d from tcgetattr", errno);
                return -1;
        }

        cfsetospeed (&tty, speed);
        cfsetispeed (&tty, speed);

        tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;     // 8-bit chars
        // disable IGNBRK for mismatched speed tests; otherwise receive break
        // as \000 chars
        tty.c_iflag &= ~IGNBRK;         // disable break processing
        tty.c_lflag = 0;                // no signaling chars, no echo,
                                        // no canonical processing
        tty.c_oflag = 0;                // no remapping, no delays
        tty.c_cc[VMIN]  = 0;            // read doesn't block
        tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

        tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

        tty.c_cflag |= (CLOCAL | CREAD);// ignore modem controls,
                                        // enable reading
        tty.c_cflag &= ~(PARENB | PARODD);      // shut off parity
        tty.c_cflag |= parity;
        tty.c_cflag &= ~CSTOPB;
        tty.c_cflag &= ~CRTSCTS;

        if (tcsetattr (fd, TCSANOW, &tty) != 0)
        {
                printf("error %d from tcsetattr", errno);
                return -1;
        }
        return 0;
}

void
set_blocking (int fd, int should_block)
{
        struct termios tty;
        memset (&tty, 0, sizeof tty);
        if (tcgetattr (fd, &tty) != 0)
        {
                printf("error %d from tggetattr", errno);
                return;
        }

        tty.c_cc[VMIN]  = should_block ? 1 : 0;
        tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

        if (tcsetattr (fd, TCSANOW, &tty) != 0)
                printf("error %d setting term attributes", errno);
}

void printATResponse(char *messageBuffer) {
    int i = 0;
    while(messageBuffer[i] != '\r') {
        i++;
    }
    if(messageBuffer[i+1] == '\n') {
        // Received message is valid
        i++;
        messageBuffer[i+1] = '\0';
        printf("%s", messageBuffer);
    } else {
        printf("Received message from the RFS board is corrupt!\n");
    }
}

void runATTerminal(int fd) {
    char ATCommand [100];
    int n = 0;
    while(scanf(" %100[^\n]", ATCommand) != 0) {
        write (fd, ATCommand, strlen(ATCommand));
        usleep ((strlen(ATCommand) + 25) * 100);
        char buf [100];
        n = read (fd, buf, sizeof buf);  // read up to 100 characters if ready to read
        // printATResponse(buf);
        for(int i = 0; i < n; i++) {
            printf("%c", buf[i]);
        }
    }
}

int main(void) {
    char *portnameMaster = "/dev/ttyUSB1";
    int fd_master = open (portnameMaster, O_RDWR | O_NOCTTY | O_SYNC);
    printf("Opened master.\n");
    if (fd_master < 0)
    {
            printf("error %d opening %s: %s", errno, portnameMaster, strerror (errno));
            return -1;
    }
    set_interface_attribs (fd_master, B38400, 0);  // set speed to 115,200 bps, 8n1 (no parity)
    set_blocking (fd_master, 0);                // set no blocking
    printf("Master Checking bluetooth connection...\n");
    checkBluetoothConnection(fd_master);
    printf("Master Resetting bluetooth...\n");
    resetBluetooth(fd_master);
    printf("Master Initializing bluetooth...\n");
    initBluetooth(fd_master);
    printf("Master The bluetooth address:\n");
    getBluetoothAddress(fd_master);
    printf("Master Setting role to master...\n");
    setRole(fd_master, 1);
    printf("Master Role:\n");
    checkRole(fd_master);
    printf("Master set CMODE = 0\n");
    setCmode(fd_master, 0);
    printf("Binding with the slave\n");
    bind(fd_master);
    setUART(fd_master, 38400);


}
// Port 1 -> ttyUSB0
// +ADDR:2018:11:202786
// Port 2 -> ttyUSB1
// +ADDR:2018:11:212162
