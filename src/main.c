#include <dep/argtable/argtable3.h>
#include <src/secure-chat/secure-chat.h>

/* global arg_xxx structs */
struct arg_lit *verb, *help, *mock_bluetooth;
struct arg_end *end;

int main(int argc, char *argv[])
{
    /* the global arg_xxx structs are initialised within the argtable */
    void *argtable[] = {
        help     = arg_litn(NULL, "help", 0, 1, "display this help and exit"),
        mock_bluetooth = arg_litn(NULL, "mock-bluetooth", 0, 1, "mock rfs bluetooth input"),
        end      = arg_end(20),
    };
    
    int exitcode = 0;
    char progname[] = "secure-chat-app";
    
    int nerrors;
    nerrors = arg_parse(argc,argv,argtable);

    /* special case: '--help' takes precedence over error reporting */
    if (help->count > 0) {
        printf("Usage: %s", progname);
        arg_print_syntax(stdout, argtable, "\n");
        printf("Options for the Secure Chat Linux App.\n\n");
        arg_print_glossary(stdout, argtable, "  %-25s %s\n");
        exitcode = 0;
        goto exit;
    }

    /* If the parser returned any errors then display them and exit */
    if (nerrors > 0) {
        /* Display the error details contained in the arg_end struct.*/
        arg_print_errors(stdout, end, progname);
        printf("Try '%s --help' for more information.\n", progname);
        exitcode = 1;
        goto exit;
    }

    if (mock_bluetooth->count == 0) {
        printf("ERROR: Real bluetooth mode not supported yet. Please use --mock-bluetooth\n");
        goto exit;
    }

    run_app();
    if (teardown_app()) {
        printf("App failed to exit cleanly\n");
    }

exit:
    /* deallocate each non-null entry in argtable[] */
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    return exitcode;
}
