#define _GNU_SOURCE
#include <stdio.h>
#include <pthread.h>


#include <src/secure-chat/secure-chat.h>
#include <src/avalon/avalon-interface/avalon-interface.h>
#include <src/avalon/lookup-table/lookup-table.h>
#include <string.h>
#include <src/aes/aes.h>
#include <src/dh/dh.h>

static int secure_chat_initialization(void)
{
    if (table_reset()) {
        return 1;
    }
    if (write_seed(0x004A3C6C, 0x000F3DCE)) {   //TODO
        return 1;
    }
    if (table_regen()) {
        return 1;
    }

    return 0;
}

static int init_app(void)
{
    if (init_avalon()) {
        return 1;
    }

    if (init_lookup_table()) {
        return 1;
    }

    //TODO: Put this in the correct place
    secure_chat_initialization();

    return 0;
}

static int encrypt_payload(uint8_t *payload, size_t size)
{
    uint32_t level2_key_index;
    if (table_lookup(&level2_key_index)) {
        return 1;
    }

    printf("Got key: %x\n", level2_key_index);
    printf("Ecrypting payload: %s\n", payload);
    //TODO: Take key index and get key. We mock here for now
    uint8_t original_key[16] = {
        0x1, 0x2, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 
        0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA
    };
    uint8_t **keyset = init_key_set(original_key);
    aes_encrypt_full_message((int)size, payload, keyset);

    printf("Ecrypted payload: %s\n", payload);
    return 0;
}

static int parse_command(char *command, size_t size)
{
    char *rw = strtok(command, " ");
    char *payload = strtok(command, " ");

    if (rw[0] == 'e') {
        // char *encrpyted = malloc(sizeof(char) * (size ))
        if (encrypt_payload(payload, size - 2)) {
            return 1;
        }
    } else if (rw[0] == 'd') {

    } else {//if(strcmp(command, "init_key_exchange")) {
        printf("Initializing key exchange...\n");
        runHostA();
        printf("Starting run hostB\n");
        // runHostB();
        
    }

    return 0;
}

int teardown_app(void)
{
    if (teardown_avalon()) {
        return 1;
    }
    // return -1; if unsuccessful perhaps?
}

int run_app(void)
{    
    // if (init_app()) {
    //     printf("Failed to init app\n");
    //     return 1;
    // }

    char *line = NULL;
    size_t size;
    // pthread_t hostAThread;
    // pthread_t hostBThread;
    while (1) {
        printf("waiting for inline\n");
        if (getline(&line, &size, stdin) == -1) {
            printf("No line\n");
        } else {
            printf("Initializing key exchange...\n");
            if(line[0] == 'm') {
                runHostA();
            } else if(line[0] == 's') {
                runHostB();
            }
            // pthread_create(&hostAThread, NULL, runHostA, NULL);
            // pthread_create(&hostBThread, NULL, runHostB, NULL);
            // pthread_join(hostAThread, NULL);
            // pthread_join(hostBThread, NULL);
        }
    }

    free(line);
    return 0;
}
